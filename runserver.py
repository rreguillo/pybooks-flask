from flask import Flask, render_template, session, redirect, url_for, escape, request

import isbndbpy as bs
import xml.etree.ElementTree as ET
from models.models import serviceBook as sB

import pdb
app = Flask(__name__)

@app.route('/')
def search():
    return render_template('search.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/detail/<isbn>')
def detail(isbn):
    #include here full details and Amazon API for image
    book = {}
    book['isbn13'] = session['isbn13']
    book['title'] = session['title']
    book['authors'] = session['authors']
    book['publisher'] = session['publisher']
    book['subjects'] = session['subjects']
    return render_template('detail.html', book=book)

@app.route('/', methods=['GET', 'POST'])
def search_item():
    if request.method == 'POST':
        try:
            assert request.form['book_title']
            result = sB(title=request.form['book_title'].replace(' ', '_'))
            session['isbn13'] = result.isbn13
            session['title'] = result.title
            session['authors'] = result.author
            session['publisher'] = result.publisher
            session['subjects'] = result.subject
            
            logging.debug(result)
        except AssertionError:
            pass
        try:
            assert request.form['book_isbn']
            result = sB(title=request.form['book_isbn'].replace(' ', '_').replace('-',''))
            session['isbn13'] = result.isbn13
            session['title'] = result.title
            session['authors'] = result.author
            session['publisher'] = result.publisher
            session['subjects'] = result.subject
            
            logging.debug(result)
        except AssertionError:
            pass
        return render_template('results.html', result=result)

        #Deprecated - isbndbpy API
        #if False:
        #    result = []
        #    req = bs.Request('books', 'combined', request.form['the_item'])
        #    req.send()
        #    res = ET.fromstring(req.send().read())
        #    if res:
        #        result = map(lambda x: '%s:%s' % (x.tag, x.attrib), res)
        #        return render_template('results.html', result=result)
    else:
        return render_template('search.html')



# set the secret key.  keep this really secret:
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True)

