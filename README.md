# README #

Flask test

### What is this repository for? ###

* Python/Flask test
* version 0.1

### SUMMARY ###

* ISBNDB queries by title/ISBN
* Custom Python wrapper for ISBNDB API (v2)
* Minimalist flask app
* Open access (demo): http://52.18.86.241

### References ###
* isbndb.com API doc: http://isbndb.com/api/v2/docs
* Flask Homepage: http://flask.pocoo.org/
* Ardit's Web App tutorial: http://pythonhow.com/building-a-website-with-python-flask/
* isbndbpy python library: https://github.com/ruiwen/isbndbpy
* Covers by openlibrary: https://openlibrary.org/

### HowTo ###
* run: 
```
#!python 

python runserver.py

```


### Who do I talk to? ###
* Raúl Reguillo Carmona (raul.reguillo@gmail.com)